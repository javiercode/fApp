import * as React from 'react'
import { Link, graphql, Text } from 'gatsby'
import Layout from '../../components/layout'

const BlogPage = ({ data }) => {
  return (
    <Layout pageTitle="My Blog Posts">
      {
        data.allMdx.nodes.map((node) => (
          <article key={node.id}>
            {console.log(node.parent)}
            <h2>
              <Link to={`/blog/${node.slug}`}>
                {node.frontmatter.title}
              </Link>
            </h2>
            <p>Posteado: {node.frontmatter.date}</p>
            <p>{"Metadata => id: "+ node.parent.id}</p>
            <p>{"Metadata => dir: "+ node.parent.dir}</p>
          </article>
          
        ))
      }
    </Layout>
  )
}

export const query = graphql`
  query {
    allMdx(sort: {fields: frontmatter___date, order: DESC}) {
      nodes {
        frontmatter {
          date(formatString: "MMMM D, YYYY")
          title
        }
        id
        slug
        rawBody
        parent {
          id
          ... on File {
            id
            name
            dir
          }
        }
      }
    }
  }
`

export default BlogPage