import * as React from 'react'
import Layout from '../components/layout'

const AboutPage = () => {
  return (
    <Layout pageTitle="Acerca de mi">
      <p>Javier Elvis Canqui Llusco.</p>
      <p>Desarrollador de software, La Paz - Bolivia 2022.</p>
    </Layout>
  )
}

export default AboutPage